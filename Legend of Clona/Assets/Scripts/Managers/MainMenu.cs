﻿using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour
{
	public void StartGame(int LevelToLoad)
	{
		Game.Instance().GetWorldManager().LoadLevel(LevelToLoad);
	}

	public void StartGame(string LevelToLoad)
	{
		Game.Instance().GetWorldManager().LoadLevel(LevelToLoad);
	}

	public void ExitGame()
	{
		#if UNITY_EDITOR
			UnityEditor.EditorApplication.isPlaying = false;
		#elif UNITY_WEBPLAYER
			Application.OpenURL(webplayerQuitURL);
		#else
			Application.Quit();
		#endif
	}
}
