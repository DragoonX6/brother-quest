﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Collections;

public class AIManager : MonoBehaviour
{
	/// <summary>
	/// The list of registered AI, used for sending messages around
	/// </summary>
	private List<BaseAI> AIList;

	/// <summary>
	/// The list containing the factories
	/// </summary>
	private static Dictionary<BaseAI.TypeID, Func<BaseAI>> factories;

	/// <summary>
	/// The message types for sending messages around
	/// </summary>
	public enum MessageType
	{
		idle,
		goTowards,
		arrived,
		tellLocation, // give away your location
		damage
	}

	static AIManager()
	{
		factories = new Dictionary<BaseAI.TypeID, Func<BaseAI>>();
	}

	/// <summary>
	/// Initialization function, unity calls this to do initialization that can't be done from the constructor
	/// </summary>
	public void Awake()
	{
		DontDestroyOnLoad(this);
		if (AIList == null)
			AIList = new List<BaseAI>();
	}

	/// <summary>
	/// Sends a message to the receiving AI
	/// </summary>
	/// <param name="sender">The AI sending the message</param>
	/// <param name="receiver">The AI receiving the message</param>
	/// <param name="messageType">The type of the message sent</param>
	/// <param name="arguments">Arguments to the message, if applicable</param>
	public void SendMessage(BaseAI sender, BaseAI receiver, MessageType messageType, EventArgs arguments)
	{
		receiver.ReceiveMessage(sender, messageType, arguments);
	}

	/// <summary>
	/// Send a message to all AI with the same type
	/// </summary>
	/// <param name="sender">The sender AI</param>
	/// <param name="type">The type of the receiving AI</param>
	/// <param name="messageType">The type of the message sent</param>
	/// <param name="arguments">Arguments to the message, if applicable</param>
	public void SendMessageToAllOfType(BaseAI sender, BaseAI.TypeID type, MessageType messageType, EventArgs arguments)
	{
		foreach(BaseAI entry in AIList)
		{
			if(entry.Type() == type)
			{
				entry.ReceiveMessage(sender, messageType, arguments);
			}
		}
	}

	/// <summary>
	/// Adds an AI to the manager.
	/// </summary>
	/// <param name="ai">AI to add to the manager</param>
	public void RegisterAI(BaseAI ai)
	{
		AIList.Add(ai);
	}

	/// <summary>
	/// Resets the manager, call this when loading a level.
	/// </summary>
	public void ResetManager()
	{
		AIList.Clear();
	}

	/// <summary>
	/// Registers a factory for spawning the AI
	/// </summary>
	/// <param name="typeID">The type id of the registering AI</param>
	/// <param name="factory">The factory delegate</param>
	public static void RegisterFactory(BaseAI.TypeID typeID, Func<BaseAI> factory)
	{
		if(!factories.ContainsKey(typeID))
		{
			factories.Add(typeID, factory);
		}
	}

	/// <summary>
	/// Returns the factory delegate of the AI if one has been registered
	/// </summary>
	/// <param name="typeID">The type id of the AI you want to spawn</param>
	/// <returns></returns>
	public static BaseAI AIFactory(BaseAI.TypeID typeID)
	{
		if(factories.ContainsKey(typeID))
		{
			return factories[typeID]();
		}
		return null;
	}
}

public class TellLocationArguments: EventArgs
{
	public Vector3 position;

	public TellLocationArguments(Vector3 position)
	{
		this.position = position;
	}
}

public class DamageArguments: EventArgs
{
	public int damage;

	public DamageArguments(int damage)
	{
		this.damage = damage;
	}
}