﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LoadingProgress : MonoBehaviour {
	private Text text;
	private string startingText;
	private float maxDistance = 100F;
	private float loadingValue;
	// Use this for initialization
	void Start () {
		text = GetComponent<Text>();
		startingText = text.text;
	}
	
	// Update is called once per frame
	void Update () {
		if (Game.Instance().GetWorldManager() != null)
		{
			loadingValue = maxDistance * Game.Instance().GetWorldManager().GetLoadingProgress(loadingValue);
			text.text = startingText + loadingValue + "%";
		}
	}
}
