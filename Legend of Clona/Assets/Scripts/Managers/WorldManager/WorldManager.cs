﻿using UnityEngine;
using System;
using System.Collections;

public class WorldManager : MonoBehaviour
{
	private AsyncOperation async;

	public void Awake()
	{
		DontDestroyOnLoad(this);
	}

	public float GetLoadingProgress(float value){
		value = async.progress;
		return value;
	}

	public void LoadLevel(String sceneName)
	{
		Application.LoadLevel(1);
		
		StartCoroutine(LoadScene(sceneName));
	}

	public void LoadLevel(int index)
	{
		Application.LoadLevel(1);
		StartCoroutine(LoadScene(index));
	}

	IEnumerator LoadScene(int name)
	{
		async = Application.LoadLevelAsync(name);
		yield return async;
	}

	IEnumerator LoadScene(string name)
	{
		async = Application.LoadLevelAsync(name);
		yield return async;
	}
}
