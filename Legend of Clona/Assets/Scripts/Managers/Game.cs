using UnityEngine;
using System.Collections;

public class Game : MonoBehaviour
{
	private static Game instance;
	private WorldManager world;
	private InputManagerController input;
	private AIManager aiManager;
	private SpawnPool spawnPool;

	public Game()
	{
		instance = this;
	}

	public static Game Instance()
	{
		if (instance == null)
			new GameObject("GameManager").AddComponent<Game>();
		return instance;
	}

	public void Awake()
	{
		DontDestroyOnLoad(this);

		if (world == null)
			world = new GameObject("WorldManager").AddComponent<WorldManager>();

		if (input == null)
			input = new GameObject("InputManager").AddComponent<InputManagerController>();

		if (aiManager == null)
			aiManager = new GameObject("AIManager").AddComponent<AIManager>();
	}

	void Update()
	{
		input.Execute();
	}

	public WorldManager GetWorldManager()
	{
		if (world == null)
			world = new GameObject("WorldManager").AddComponent<WorldManager>();
		return world;
	}

	/* public AudioManager GetAudioManager()
	 * {
	 *	if (audio == null)
			audio = new GameObject("AudioManager").AddComponent<AudioManager>();
		return audio;
	 * } 
	 */

	public InputManagerController GetInputManager()
	{
		if (input == null)
			input = new GameObject("InputManager").AddComponent<InputManagerController>();
		return input;
	}

	public AIManager GetAIManager()
	{
		if (aiManager == null)
			aiManager = new GameObject("AIManager").AddComponent<AIManager>();
		return aiManager;
	}

	public SpawnPool GetSpawnpool()
	{
		if (spawnPool == null)
			spawnPool = new GameObject("Spawn pool").AddComponent<SpawnPool>();
		return spawnPool;
	}

	public void RegisterSpawnPool(SpawnPool pool)
	{
		if (spawnPool == null)
			Debug.Log("Registered new spawn pool!");
		else
			Debug.Log("Overriding old spawn pool!");
		spawnPool = pool;
	}
}
