﻿using UnityEngine;
using System.Collections;

public class BossButtonManager : MonoBehaviour {

	private BossButton[] buttons = new BossButton[2];
	public GameObject bossDoor;
	// Use this for initialization
	void Start () {
		for (int i = 0; i < GameObject.FindObjectsOfType<BossButton>().Length; i++)
		{
			buttons[i] = GameObject.FindObjectsOfType<BossButton>()[i];
		}
	}
	
	// Update is called once per frame
	void Update () {
		foreach (BossButton b in buttons)
		{
			if (b.pressed && bossDoor.gameObject.activeInHierarchy)
			{
				bossDoor.gameObject.SetActive(false);
			}
		}
	}
}
