﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpawnPool : MonoBehaviour
{
	// bool checks if spawned
	private Dictionary<BaseAI.TypeID, List<Tuple<BaseAI, bool>>> pool = new Dictionary<BaseAI.TypeID, List<Tuple<BaseAI, bool>>>();

	void Awake()
	{
		Game.Instance().RegisterSpawnPool(this);
	}

	void Start()
	{
		Deserialize();
	}

	#region Serialization
	public List<BaseAI.TypeID> _types = new List<BaseAI.TypeID>();
	public List<SerializeTupleList> _tuples = new List<SerializeTupleList>();

	//public void Serialize()
	//{
	//	Debug.Log("Serializing");
	//	_types.Clear();
	//	_tuples.Clear();
	//	foreach(KeyValuePair<BaseAI.TypeID, List<Tuple<BaseAI, bool>>> entry in pool)
	//	{
	//		_types.Add(entry.Key);
	//		SerializeTupleList STL = new SerializeTupleList();
	//		foreach (Tuple<BaseAI, bool> listEntry in entry.Value)
	//		{
	//			SerializeTuple temp = new SerializeTuple()
	//			{
	//				spawnObject = listEntry.Item1,
	//				spawned = listEntry.Item2
	//			};
				
	//			STL._values.Add(temp);
	//		}
	//		_tuples.Add(STL);
	//	}
	//}

	public void Deserialize()
	{
		pool = new Dictionary<BaseAI.TypeID, List<Tuple<BaseAI, bool>>>();
		for(int i = 0; i < _types.Count; ++i)
		{
			List<Tuple<BaseAI, bool>> temp = new List<Tuple<BaseAI, bool>>();
			for(int j = 0; j < _tuples[i]._values.Count; ++j)
			{
				temp.Add(Tuple.Create(_tuples[i]._values[j].spawnObject, _tuples[i]._values[j].spawned));
			}
			pool.Add(_types[i], temp);
		}
	}
	#endregion

	public void Get(BaseAI.TypeID typeID, Vector3 position, int count)
	{
		if(pool.ContainsKey(typeID))
		{
			foreach(Tuple<BaseAI, bool> entry in pool[typeID])
			{
				if(!entry.Item2)
				{
					entry.Item1.enabled = true;
					entry.Item1.transform.position = position;
					if (--count <= 0)
						return;
				}
			}

			for(; count > 0; --count)
			{
				BaseAI temp = AIManager.AIFactory(typeID);
				if(temp == null)
				{
					Debug.LogError("Error: No factory found for " + typeID);
					return;
				}
				temp.transform.position = position;
				pool[typeID].Add(Tuple.Create(temp, true));
			}
		}
		else
		{
			pool.Add(typeID, new List<Tuple<BaseAI, bool>>());
			for(; count > 0; --count)
			{
				BaseAI temp = AIManager.AIFactory(typeID);
				if(temp == null)
				{
					Debug.LogError("Error: No factory found for " + typeID);
				}
				temp.transform.position = position;
				pool[typeID].Add(Tuple.Create(temp, true));
			}
		}
	}

	public void PutBack(BaseAI spawnedObject)
	{
		BaseAI.TypeID typeID = spawnedObject.Type();
		if(pool.ContainsKey(typeID))
		{
			int index = pool[typeID].FindIndex(x => x.Item1 == spawnedObject);
			if(index > 0)
			{
				Tuple<BaseAI, bool> temp = pool[typeID][index];
				temp.Item1.transform.position = transform.position;
				temp.Item2 = false;
				if(temp.Item1.rigidbody)
				{
					temp.Item1.rigidbody.Sleep();
				}
				temp.Item1.enabled = false;
				pool[typeID][index] = temp;
			}
			return;
		}
		pool.Add(typeID, new List<Tuple<BaseAI, bool>>());
		spawnedObject.enabled = false;
		spawnedObject.transform.position = transform.position;
		if (spawnedObject.rigidbody)
			spawnedObject.rigidbody.Sleep();
		pool[typeID].Add(Tuple.Create(spawnedObject, false));
	}
}

[System.Serializable]
public class SerializeTuple
{
	public BaseAI spawnObject;
	public bool spawned;
}

[System.Serializable]
public class SerializeTupleList
{
	public List<SerializeTuple> _values = new List<SerializeTuple>();
}
