﻿using UnityEngine;
using System.Collections;

public class Torch : InteractableObject
{
	public GameObject torchLight;
	public GameObject mainFlame;
	public GameObject baseFlame;
	public GameObject etincelles;
	public GameObject fumee;
	public float maxLightIntensity;
	public float intensityLight = 0.5652174F;
	public bool lighted = false;

	public override void Interact(BaseAI interact)
	{

	}

	void Update()
	{
		if (lighted)
		{
			if (intensityLight < 0) intensityLight = 0;
			if (intensityLight > maxLightIntensity) intensityLight = maxLightIntensity;

			torchLight.light.intensity = intensityLight * 0.5F + Mathf.Lerp(intensityLight - 0.1f, intensityLight + 0.1f, Mathf.Cos(Time.time * 30));

			torchLight.light.color = new Color(Mathf.Min(intensityLight * 0.666667F, 1f), Mathf.Min(intensityLight * 0.5F, 1f), 0f);
			mainFlame.GetComponent<ParticleSystem>().emissionRate = intensityLight * 20f;
			baseFlame.GetComponent<ParticleSystem>().emissionRate = intensityLight * 15f;
			etincelles.GetComponent<ParticleSystem>().emissionRate = intensityLight * 7f;
			fumee.GetComponent<ParticleSystem>().emissionRate = intensityLight * 12f;
		}

	}

	public override TypeID Type()
	{
		return TypeID.torch;
	}

	public void OnTriggerEnter(Collider other)
	{
		InteractableObject o = other.gameObject.GetComponent<InteractableObject>();
		if (o) 
		{
			switch (o.Type())
			{
				case InteractableObject.TypeID.fireArrow:
				{
					lighted = true;
				} break;
			}
		}
	}

	public void OnTriggerExit(Collider other)
	{

	}

}
