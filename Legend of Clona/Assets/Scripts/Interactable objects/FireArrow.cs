﻿using UnityEngine;
using System.Collections;

public class FireArrow : Arrow
{
	public int fireDamage = 2;

	public GameObject flame;

	public override void Interact(BaseAI interact)
	{
	}

	public override TypeID Type()
	{
		return TypeID.fireArrow;
	}

	public override void OnTriggerEnter(Collider other)
	{
		InteractableObject interactableCollider = other.gameObject.GetComponent<InteractableObject>();
		BaseAI baseCollider = other.gameObject.GetComponent<BaseAI>();
		if (baseCollider != null)
		{
			switch (baseCollider.Type())
			{
				case BaseAI.TypeID.boss:
				{
					//take some HP from the boss. (Boss HP - damage - fireDamage)
					if (player)
					{
						Game.Instance().GetAIManager().SendMessage(player, baseCollider, AIManager.MessageType.damage, new DamageArguments(fireDamage + damage));
					}
				} break;
				case BaseAI.TypeID.minion:
				{
					//take some HP from the minion. (Minion HP - damage - fireDamage)
					if(player)
					{
						Game.Instance().GetAIManager().SendMessage(player, baseCollider, AIManager.MessageType.damage, new DamageArguments(fireDamage + damage));
					}
				}break;
			}
		}

		if (interactableCollider != null)
		{
			if (interactableCollider.Type() != InteractableObject.TypeID.torch)
			{
				rigidbody.isKinematic = true;
				flame.SetActive(false);
			}
		}
		else
		{
			rigidbody.isKinematic = true;
			flame.SetActive(false);
		}

	}
}
