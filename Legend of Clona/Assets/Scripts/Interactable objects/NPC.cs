﻿using UnityEngine;
using System.Collections;

public class NPC : InteractableObject
{
	public override void Interact(BaseAI interact)
	{
	}
	public override TypeID Type()
	{
		return TypeID.NPC;
	}

	public void OnTriggerEnter(Collider other)
	{
		BaseAI o = other.gameObject.GetComponent<BaseAI>();
		if (o != null)
		{
			switch (o.Type())
			{
				case BaseAI.TypeID.player:
				{
				} break;
			}
		}
	}

	public void OnTriggerExit(Collider other)
	{

	}
}
