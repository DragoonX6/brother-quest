﻿using UnityEngine;
using InControl;
using System.Collections;

public class Sword : MonoBehaviour {

	public int damage;
	public Animator ani;

	private InputDevice inputDevice;
	private bool isAttacking;

	private Player player;

	void Awake()
	{
		player = FindObjectOfType<Player>();
	}

	void Start()
	{
		inputDevice = InputManager.ActiveDevice;
	}

	void Update()
	{
		if (inputDevice.Action3 && !isAttacking)
		{
			ani.SetBool("Idle", false);
			isAttacking = true;
			StartCoroutine(WaitForAttackFinish(0.5F));		
		}
	}

	IEnumerator WaitForAttackFinish(float seconds)
	{
		ani.SetBool("Attack", true);
		yield return new WaitForSeconds(seconds);
		ani.SetBool("Attack", false);
		ani.SetBool("Idle", true);
		isAttacking = false;
	}

	public virtual void OnTriggerEnter(Collider other)
	{
		//set some variables so that I can use them.
		BaseAI enemy = other.gameObject.GetComponent<BaseAI>();

		//check what kind of BaseAI has been touched.
		if (enemy != null)
		{
			switch (enemy.Type())
			{
				case BaseAI.TypeID.player:
				{

				} break;
				case BaseAI.TypeID.boss:
				{
					//take some HP from the boss. (Boss HP - damage)
					Game.Instance().GetAIManager().SendMessage(player, enemy, AIManager.MessageType.damage, new DamageArguments(damage));
				} break;
				case BaseAI.TypeID.minion:
				{
					//take some HP from the minion. (Minion HP - damage)
					Game.Instance().GetAIManager().SendMessage(player, enemy, AIManager.MessageType.damage, new DamageArguments(damage));
				} break;
			}
		}
	}
}
