﻿using UnityEngine;
using System.Collections;

public abstract class InteractableObject : MonoBehaviour
{
	private TypeID type;

	public enum TypeID
	{
		torch,
		checst,
		NPC,
		arrow,
		fireArrow
	}

	public abstract void Interact(BaseAI interact);

	public abstract TypeID Type();
}
