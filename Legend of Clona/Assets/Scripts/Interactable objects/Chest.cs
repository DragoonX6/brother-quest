﻿using UnityEngine;
using System.Collections;

public class Chest : InteractableObject
{
	public Animator animator;

	public void Awake()
	{

	}

	public override void Interact(BaseAI interact)
	{
		if (interact.Type() == BaseAI.TypeID.player)
		{
			animator.SetBool("Opened", true);
		}
	}
	public override TypeID Type()
	{
		return TypeID.checst;
	}

	public void OnTriggerExit(Collider other)
	{

	}

	public void OnTriggerEnter(Collider other)
	{
		BaseAI o = other.gameObject.GetComponent<BaseAI>();
		if (o != null)
		{
			switch (o.Type())
			{
				case BaseAI.TypeID.player:
				{
					//opened = true;
				} break;
			}
		}
	}
}
