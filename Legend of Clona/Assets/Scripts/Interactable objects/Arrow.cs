﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof (Rigidbody))]

public class Arrow : InteractableObject
{
	public int damage = 5;

	protected Player player;

	void Awake()
	{
		player = FindObjectOfType<Player>();
	}

	void Start()
	{
		rigidbody.isKinematic = true;
	}

	public override void Interact(BaseAI interact)
	{
	}

	//Set what the type this InteractableObject is.
	public override TypeID Type()
	{
		return TypeID.arrow;
	}

	public virtual void OnTriggerEnter(Collider other)
	{
		//set some variables so that I can use them.
		InteractableObject interactableCollider = other.gameObject.GetComponent<InteractableObject>();
		BaseAI o = other.gameObject.GetComponent<BaseAI>();

		//check what kind of BaseAI has been touched.
		if (o != null)
		{
			switch (o.Type())
			{
				case BaseAI.TypeID.player:
				{

				} break;
				case BaseAI.TypeID.boss:
				{
					//take some HP from the boss. (Boss HP - damage)
				} break;
				case BaseAI.TypeID.minion:
				{
					//take some HP from the minion. (Minion HP - damage)
				} break;
			}
		}

		//Check what has been hit. If the torch has been hit the arrow will keep flying else it will stop flying.
		if (interactableCollider != null)
		{
			if (interactableCollider.Type() != InteractableObject.TypeID.torch)
			{
				rigidbody.isKinematic = true;
				DestoryArrow();
			}
		}
		else
		{
			rigidbody.isKinematic = true;
			DestoryArrow();
		}
	}

	public void DestoryArrow()
	{
		Destroy(this.gameObject);
	}

	IEnumerator StartCountdown()
	{
		yield return new WaitForSeconds(10);
		DestoryArrow();
	}

	//Make sure the physic for the arrow work.
	public virtual void FixedUpdate()
	{
		if (!rigidbody.isKinematic)
			RotateToVelocity(10F, false);
	}

	//Test method for me to fire the arrow.
	public virtual void FireArrowForward(float speed)
	{
		transform.parent = null;
		rigidbody.isKinematic = false;
		var scaleSwapping = transform.localScale.z;
		transform.localScale = new Vector3(transform.localScale.x, transform.localScale.y, -scaleSwapping);
		rigidbody.AddForce(-transform.forward * 1000 * speed);

		StartCoroutine(StartCountdown());
	}

	//The actual method to do the physics for the arrow.
	public virtual void RotateToVelocity(float turnSpeed, bool ignoreY)
	{
		Vector3 dir;
		if (ignoreY)
			dir = new Vector3(rigidbody.velocity.x, 0f, rigidbody.velocity.z);
		else dir = rigidbody.velocity;

		if (dir.magnitude > 0.1F)
		{
			Quaternion dirQ = Quaternion.LookRotation(dir);
			Quaternion slerp = Quaternion.Slerp(transform.rotation, dirQ, dir.magnitude * turnSpeed * Time.deltaTime);
			rigidbody.MoveRotation(slerp);
		}
	}
}
