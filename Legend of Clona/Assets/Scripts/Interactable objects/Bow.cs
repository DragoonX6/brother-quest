﻿using UnityEngine;
using InControl;
using System.Collections;

public class Bow : MonoBehaviour
{
	public float speed;
	public Transform reloadPosition;
	public Transform arrowParent;
	public Transform lookAtPos;
	public GameObject arrowPrefab;
	public Animator ani;

	private InputDevice inputDevice;
	private Arrow loadedArrow;

	void Start()
	{
		inputDevice = InputManager.ActiveDevice;
	}

	void Update()
	{
		arrowParent.LookAt(lookAtPos);

		if (inputDevice.RightTrigger.WasReleased && loadedArrow != null)
			FireBow();

		if (inputDevice.Action3 && !loadedArrow && !inputDevice.RightTrigger.IsPressed)
			StartCoroutine(ReloadArrow(reloadPosition));

		ani.SetFloat("chargeValue", inputDevice.RightTrigger);
	}
	void FireBow()
	{
		if(loadedArrow)
			loadedArrow.FireArrowForward(ani.GetFloat("chargeValue"));

		loadedArrow = null;
	}

	IEnumerator ReloadArrow(Transform spawnPosition)
	{
		(Instantiate(arrowPrefab, spawnPosition.position, spawnPosition.rotation) as GameObject).transform.parent = spawnPosition.parent;
		InteractableObject interactableCollider = arrowParent.gameObject.GetComponentInChildren<InteractableObject>();

		if (interactableCollider != null)
		{
			if (interactableCollider.Type() == InteractableObject.TypeID.arrow)
				loadedArrow = interactableCollider.gameObject.GetComponent<Arrow>();
			else if (interactableCollider.Type() == InteractableObject.TypeID.fireArrow)
				loadedArrow = interactableCollider.gameObject.GetComponent<FireArrow>();
		}
		else
		{
			Debug.Log("An Arrow could not be found");
			StopAllCoroutines();
		}

		yield return loadedArrow;

		loadedArrow.transform.parent = reloadPosition.parent;
		loadedArrow.rigidbody.isKinematic = true;
	}
}
