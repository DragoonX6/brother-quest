﻿using UnityEngine;
using System.Collections;

public class PressurePlateController : MonoBehaviour
{
	public GameObject[] plates;
	public GameObject[] otherPlates;
	public GameObject endSpikes;
	public GameObject endChest;
	public Transform playerStartPoint;
	public static PressurePlateController instance;

	private int currentPlate;
	private Vector3[] plateStartpoints;
	private GameObject player;

	void Start()
	{
		instance = this;

		plateStartpoints = new Vector3[plates.Length + otherPlates.Length];

		for (int i = 0; i < plates.Length + otherPlates.Length; i++)
		{
			if (i < plates.Length)
			{
				plateStartpoints[i] = plates[i].transform.position;
				plates[i].GetComponent<PressurePlate>().plateNumber = i;
			}
			else if (i >= plates.Length)
				plateStartpoints[i] = otherPlates[i - plates.Length].transform.position;
		}
	}

	void Update()
	{
		if (currentPlate == plates.Length)
			Victory();
	}

	public bool CheckForRightTile(GameObject p, int tileNumber)
	{
		player = p;

		if (tileNumber == currentPlate)
		{
			currentPlate++;
			return true;
		}
		else if (tileNumber > -1 && tileNumber < currentPlate)
			return true;
		else
			return false;
	}

	public void ResetGame()
	{
		PressurePlate.instance.startMoving = false;

		for (int i = 0; i < plates.Length + otherPlates.Length; i++)
		{
			if (i < plates.Length)
			{
				plates[i].GetComponent<PressurePlate>().startMoving = false;
				plates[i].transform.position = plateStartpoints[i];
			}
			else if (i >= plates.Length)
			{
				otherPlates[i - plates.Length].GetComponent<PressurePlate>().startMoving = false;
				otherPlates[i - plates.Length].gameObject.transform.position = plateStartpoints[i];
			}
		}

		player.transform.position = playerStartPoint.position;
		player.transform.rotation = playerStartPoint.rotation;

		currentPlate = 0;
	}

	void Victory()
	{
		for (int i = 0; i < endSpikes.transform.childCount; i++)
		{
			Vector3 move = new Vector3(endSpikes.transform.GetChild(i).transform.position.x, endSpikes.transform.GetChild(i).transform.position.y - endSpikes.transform.GetChild(i).transform.localScale.y, endSpikes.transform.GetChild(i).transform.position.z);
			Vector3 lerp = Vector3.Lerp(endSpikes.transform.GetChild(i).transform.position, move, 5F * Time.deltaTime);
			endSpikes.transform.GetChild(i).rigidbody.MovePosition(lerp);
		}

		endChest.collider.enabled = true;
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject == player)
		{
			ResetGame();
		}
	}

}
