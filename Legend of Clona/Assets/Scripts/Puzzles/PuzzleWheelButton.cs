﻿using UnityEngine;
using System.Collections;

public class PuzzleWheelButton : MonoBehaviour
{
	public GameObject ringToRotate;
	public float moveDistance;
	public float moveSpeed;
	public float rotationSpeed;

	private bool startRotating;
	private float buttonStartPos;
	private Vector3 rotateValue, positionValue;

	void Start()
	{
		positionValue = transform.position;
		buttonStartPos = positionValue.y;
	}

	void OnTriggerEnter(Collider other)
	{
		BaseAI o = other.gameObject.GetComponent<BaseAI>();
		if (o != null)
		{
			switch (o.Type())
			{
				case BaseAI.TypeID.player:
				{
					if (!startRotating)
					{
						startRotating = true;

						float roundedValue = ringToRotate.transform.rotation.eulerAngles.z + 120;
						roundedValue = (Mathf.Round(roundedValue / 10)) * 10;

						if (roundedValue < 360)
						{
							rotateValue = new Vector3(0, 0, roundedValue);
						}
						else
						{
							float remainingValue = roundedValue - 360;
							rotateValue = new Vector3(0, 0, remainingValue);
						}
					}
				} break;
			}
		}

	}

	void Update()
	{
		if ((ringToRotate.transform.rotation.eulerAngles.z < rotateValue.z - 0.2F || ringToRotate.transform.rotation.eulerAngles.z > rotateValue.z + 0.2F)
			&& rotateValue != Vector3.zero)
		{
			ringToRotate.transform.Rotate(Vector3.forward * rotationSpeed * Time.deltaTime);
		}
		else
		{
			startRotating = false;
		}
		//if (transform.position.y < positionValue.y - 0.02F || transform.position.y > positionValue.z + 0.02F)
			//transform.Translate(positionValue * moveSpeed * Time.deltaTime);
		if (startRotating)
			positionValue.y = moveDistance;
		else
			positionValue.y = buttonStartPos;

		transform.position = Vector3.MoveTowards(transform.position, positionValue, moveSpeed * Time.deltaTime);
	}
}
