﻿using UnityEngine;
using System.Collections;

public class PuzzleWheel : MonoBehaviour
{

	public GameObject innerButton, middleButton, outerButton;
	public GameObject innerRing, middleRing, outerRing;
	public float innerValue, middleValue, outerValue;

	public static PuzzleWheel instance;

	// Use this for initialization
	void Start()
	{
		instance = this;
	}

	// Update is called once per frame
	void Update()
	{
		float iR = innerRing.transform.rotation.eulerAngles.z;
		iR = (Mathf.Round(iR / 10)) * 10;

		float mR = middleRing.transform.rotation.eulerAngles.z;
		mR = (Mathf.Round(mR / 10)) * 10;

		float oR = outerRing.transform.rotation.eulerAngles.z;
		oR = (Mathf.Round(oR / 10)) * 10;

		if (iR == innerValue)
			if (mR == middleValue)
				if (oR == outerValue)
					Debug.Log("You won!");
	}


}
