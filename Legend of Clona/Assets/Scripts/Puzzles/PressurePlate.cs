﻿using UnityEngine;
using System.Collections;

public class PressurePlate : MonoBehaviour {

	internal int plateNumber;
	public float moveDistance;
	internal bool startMoving;
	public static PressurePlate instance;

	void Awake()
	{
		instance = this;

		plateNumber = -1;
	}

	void Update()
	{
		if (startMoving)
		{
			Vector3 move = new Vector3(transform.position.x, transform.position.y - moveDistance, transform.position.z);
			Vector3 lerp = Vector3.Lerp(transform.position, move,  0.15F * Time.deltaTime);
			rigidbody.MovePosition(lerp);
		}
	}
	void OnTriggerEnter(Collider other)
	{
		BaseAI o = other.gameObject.GetComponent<BaseAI>();
		if (o != null)
		{
			switch (o.Type())
			{
				case BaseAI.TypeID.player:
				{
					if (!PressurePlateController.instance.CheckForRightTile(o.gameObject, plateNumber))
					{
						startMoving = true;
					}
				} break;
			}
		}
	}
}
