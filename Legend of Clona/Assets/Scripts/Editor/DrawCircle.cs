﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(Player))]
public class DrawCircle : Editor
{
	void OnSceneGUI()
	{
		Handles.color = Color.red;
		Player myTarget = target as Player;

		Handles.DrawWireDisc(myTarget.transform.position, Camera.main.transform.right, Vector3.Distance(myTarget.transform.position, Camera.main.transform.position));
	}
}
