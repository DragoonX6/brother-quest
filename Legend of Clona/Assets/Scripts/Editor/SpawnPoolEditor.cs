﻿using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[CustomEditor(typeof(SpawnPool))]
public class SpawnPoolEditor : Editor
{
	private bool showPool = false;
	private List<bool> showElement = new List<bool>();

	public override void OnInspectorGUI()
	{
		//base.OnInspectorGUI();

		SpawnPool mySpawnPool = target as SpawnPool;
		showPool = EditorGUILayout.Foldout(showPool, "Pool");
		if (showPool)
		{
			int poolSize = EditorGUILayout.IntField("Size", mySpawnPool._types.Count);

			if (poolSize < 0)
				poolSize = 0;

			if (showElement.Count != mySpawnPool._types.Count)
			{
				if (showElement.Count < mySpawnPool._types.Count)
				{
					int toAdd = mySpawnPool._types.Count - showElement.Count;
					for (; toAdd > 0; --toAdd)
						showElement.Add(new bool());
				}
				else
				{
					int toRemove = showElement.Count - mySpawnPool._types.Count;
					for (; toRemove > 0; --toRemove)
						showElement.RemoveAt(showElement.Count - 1);
				}
			}

			if (poolSize != mySpawnPool._types.Count)
			{
				if (poolSize < mySpawnPool._types.Count)
				{
					int toRemove = mySpawnPool._types.Count - poolSize;
					for (; toRemove > 0; --toRemove)
					{
						mySpawnPool._types.RemoveAt(mySpawnPool._types.Count - 1);
						mySpawnPool._tuples.RemoveAt(mySpawnPool._tuples.Count - 1);
						showElement.RemoveAt(showElement.Count - 1);
					}
				}
				else
				{
					int toAdd = poolSize - mySpawnPool._types.Count;
					for (; toAdd > 0; --toAdd)
					{
						mySpawnPool._types.Add(new BaseAI.TypeID());
						mySpawnPool._tuples.Add(new SerializeTupleList());
						showElement.Add(new bool());
					}
				}
			}

			for (int i = 0; i < poolSize; ++i)
			{
				showElement[i] = EditorGUILayout.Foldout(showElement[i], "Element " + i);

				if (showElement[i])
				{
					mySpawnPool._types[i] = (BaseAI.TypeID)EditorGUILayout.EnumPopup("Type", mySpawnPool._types[i]);
					int valueSize = EditorGUILayout.IntField("Size", mySpawnPool._tuples[i]._values.Count);

					if (valueSize < 0)
						valueSize = 0;

					if (valueSize != mySpawnPool._tuples[i]._values.Count)
					{
						if (valueSize > mySpawnPool._tuples[i]._values.Count)
						{
							int toAdd = valueSize - mySpawnPool._tuples[i]._values.Count;
							for (; toAdd > 0; --toAdd)
							{
								mySpawnPool._tuples[i]._values.Add(new SerializeTuple());
								mySpawnPool._tuples[i]._values[mySpawnPool._tuples[i]._values.Count - 1].spawned = true;
							}
						}
						else
						{
							int toRemove = mySpawnPool._tuples[i]._values.Count - valueSize;
							for (; toRemove > 0; --toRemove)
								mySpawnPool._tuples[i]._values.RemoveAt(mySpawnPool._tuples[i]._values.Count - 1);
						}
					}

					for (int j = 0; j < mySpawnPool._tuples[i]._values.Count; ++j)
						mySpawnPool._tuples[i]._values[j].spawnObject = (BaseAI)EditorGUILayout.ObjectField("SpawnObject", mySpawnPool._tuples[i]._values[j].spawnObject, typeof(BaseAI), true);
				}
			}
		}
	}
}
