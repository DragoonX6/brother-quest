﻿using UnityEngine;
using System;
using System.Collections;

public abstract class BaseAI : MonoBehaviour
{
	public int health;
	public float movementSpeed;
	public float jumpHeight;
	protected State currentState;

	public enum TypeID
	{
		player,
		minion,
		boss,
		test
	}

	public enum State
	{
		idle,
		chase,
		attacking,
		returning
	}

	/// <summary>
	/// Initialization function of Unity, call this from deriving classes to register it with the AI manager
	/// </summary>
	public virtual void Awake()
	{
		Game.Instance().GetAIManager().RegisterAI(this);
	}

	/// <summary>
	/// Gets called when a message has been send to this by another AI
	/// </summary>
	/// <param name="sender">The AI sending the message</param>
	/// <param name="messageType">The type of the message that has been send</param>
	/// <param name="arguments">Arguments to the message, if applicable</param>
	public abstract void ReceiveMessage(BaseAI sender, AIManager.MessageType messageType, EventArgs arguments);

	/// <summary>
	/// Get the type id of the AI
	/// </summary>
	/// <returns>Assigned type id of current AI</returns>
	public abstract TypeID Type();
}
