﻿using UnityEngine;
using System.Collections;

public class TestAI : BaseAI
{
	static TestAI()
	{
		AIManager.RegisterFactory(TypeID.test, Factory);
	}

	public static TestAI Factory()
	{
		return (TestAI)Instantiate(Resources.Load<TestAI>("Test AI"));
	}

	public override void Awake()
	{
		base.Awake();
	}

	public override void ReceiveMessage(BaseAI sender, AIManager.MessageType messageType, System.EventArgs arguments)
	{
		//throw new System.NotImplementedException();
	}

	public override TypeID Type()
	{
		return TypeID.test;
	}

	public void Update()
	{
		if (transform.position.y <= -20.0f)
		{
			Game.Instance().GetSpawnpool().PutBack(this);
		}
	}
}
