﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(NavMeshAgent))]
public class Minion : BaseAI
{
	private Vector3 targetLocation;
	public LayerMask layerMask;
	private Vector3 spawnPoint;
	public float idleRadius;
	public float playerDetectionRange;
	private State state = State.idle;
	private NavMeshAgent navAgent;

	public int damage;
	private float hitTimeOut;
	public float hitTimeOutMax;

	public float stateTimeoutMax;
	private float stateTimeout;

	/// <summary>
	/// Static initializer, register factory here
	/// </summary>
	static Minion()
	{
		AIManager.RegisterFactory(TypeID.minion, Factory);
	}

	/// <summary>
	/// Initialize variables here, called when the object is created
	/// </summary>
	public override void Awake()
	{
		base.Awake();
		targetLocation.x = Mathf.Infinity;
		navAgent = GetComponent<NavMeshAgent>();
		spawnPoint = transform.position;
	}

	public void Update()
	{
		switch(state)
		{
			case State.idle:
			{
				IdleRoutine();
			}break;
			case State.chase:
			{
				ChaseRoutine();
			}break;
			case State.returning:
			{
				ReturnRoutine();
			}break;
		}

		if(health <= 0)
		{
			state = State.idle;
			navAgent.enabled = false;
			Game.Instance().GetSpawnpool().PutBack(this);
		}

		hitTimeOut += Time.deltaTime;
	}

	/// <summary>
	/// idle behavior
	/// </summary>
	private void IdleRoutine()
	{
		if (targetLocation.x != Mathf.Infinity)
		{
			RaycastHit hit;
			Vector3 direction = targetLocation - transform.position;
			direction.Normalize();
			if (Physics.Raycast(transform.position, direction, out hit, playerDetectionRange, layerMask.value))
			{
				Player player = hit.collider.gameObject.GetComponent<Player>();
				if (player)
				{
					targetLocation = player.transform.position;
					state = State.chase;
					Debug.Log("State switch to " + state);
				}
			}
		}
	}

	/// <summary>
	/// Chase the player, attack if we're close enough
	/// </summary>
	private void ChaseRoutine()
	{
		if(targetLocation.x !=  Mathf.Infinity)
		{
			if (navAgent.enabled)
			{
				navAgent.SetDestination(targetLocation);
				if (Vector3.Distance(transform.position, targetLocation) > playerDetectionRange)
				{
					state = State.returning;
					Debug.Log("State switch to " + state);
				}
			}
		}
	}

	/// <summary>
	/// Go back to the spawn
	/// </summary>
	private void ReturnRoutine()
	{
		if(navAgent.enabled)
		{
			navAgent.SetDestination(spawnPoint);
			
			if (targetLocation.x != Mathf.Infinity)
			{
				RaycastHit hit;
				Vector3 direction = targetLocation - transform.position;
				direction.Normalize();
				Debug.DrawLine(transform.position, transform.position + direction * 10, Color.magenta);
				if (Physics.Raycast(transform.position, direction, out hit, playerDetectionRange, layerMask.value))
				{
					Player player = hit.collider.gameObject.GetComponent<Player>();
					if (player && stateTimeout >= stateTimeoutMax)
					{
						targetLocation = player.transform.position;
						state = State.chase;
						Debug.Log("State switch to " + state);
						stateTimeout = 0;
						return;
					}
				}
			}

			if(Vector3.Distance(transform.position, spawnPoint) < 5f)
			{
				state = State.idle;
				stateTimeout = 0;
				Debug.Log("State switch to " + state);
				return;
			}

			stateTimeout += Time.deltaTime;
		}
	}

	/// <summary>
	/// Factory function for the minion
	/// </summary>
	/// <returns>New instance of Minion</returns>
	static Minion Factory()
	{
		// TODO!!!
		return (Minion)Instantiate(Resources.Load<Minion>("Minion"));
	}

	/// <summary>
	/// Gets called when a message has been send to this by another AI
	/// </summary>
	/// <param name="sender">The AI sending the message</param>
	/// <param name="messageType">The type of the message that has been send</param>
	/// <param name="arguments">Arguments to the message, if applicable</param>
	public override void ReceiveMessage(BaseAI sender, AIManager.MessageType messageType, System.EventArgs arguments)
	{
		if(sender.Type() == TypeID.player)
		{
			switch(messageType)
			{
				case AIManager.MessageType.tellLocation:
				{
					targetLocation = ((TellLocationArguments)arguments).position;
				}break;
				case AIManager.MessageType.damage:
				{
					health -= ((DamageArguments)arguments).damage;
				}break;
			}
		}
	}

	/// <summary>
	/// Get the type of the minion
	/// </summary>
	/// <returns>TypeID.minion</returns>
	public override TypeID Type()
	{
		return TypeID.minion;
	}

	public void OnCollisionEnter(Collision collision)
	{
		Player player = collision.gameObject.GetComponent<Player>();
		if(player != null)
		{
			if(player.Type() == TypeID.player)
			{
				if (hitTimeOut >= hitTimeOutMax)
				{
					Game.Instance().GetAIManager().SendMessage(this, player, AIManager.MessageType.damage, new DamageArguments(damage));
					hitTimeOut = 0;
				}
			}
		}
	}
}
