using UnityEngine;
using System.Collections;

public partial class Player : BaseAI
{
	#region input
	/// <summary>
	/// Function bound to the left stick's X axis
	/// </summary>
	/// <param name="value">The X position of the stick</param>
	public void OnLeftStickX(float value)
	{
		if (value >= leftStickXDeadZone || value <= -leftStickXDeadZone)
		{
			transform.Rotate(transform.up, value * playerRotationSpeed * Time.deltaTime);
			//transform.FindChild("Bip001").Rotate(transform.up, value * playerRotationSpeed * Time.deltaTime * 1000);
		}

		//animationController.SetFloat(leftJoyXID, value);
	}

	/// <summary>
	/// Function bound to the left stick's Y axis
	/// </summary>
	/// <param name="value">The Y position of the stick</param>
	public void OnLeftStickY(float value)
	{
		if (value >= leftStickYDeadZone || value <= -leftStickYDeadZone)
		{
			transform.Translate(Vector3.forward * value * movementSpeed * Time.deltaTime);
		}
		//animationController.SetFloat(leftJoyYID, value);
	}

	/// <summary>
	/// Function bound to the right stick's X axis
	/// </summary>
	/// <param name="value">The X position of the stick</param>
	public void OnRightStickX(float value)
	{
		if (value >= rightStickXDeadZone || value <= -rightStickXDeadZone)
		{
			if (thirdPersonCamera)
			{
				if (thirdPersonCamera.gameObject.activeInHierarchy)
				{
					if (cameraIsAttached)
					{
						DetachCameraAngles();
					}
					cameraIsAttached = false;

					cameraYAngle += value * cameraRotationSpeedX * Time.deltaTime;

					while (cameraYAngle >= 360f)
						cameraYAngle -= 360f;
					while (cameraYAngle <= 0f)
						cameraYAngle += 360f;
				}
			}
			else if (firstPersonCamera.gameObject.activeInHierarchy)
			{
				transform.Rotate(0, value * cameraRotationSpeedX * Time.deltaTime, 0);
			}
		}
	}

	/// <summary>
	/// Function bound to the right stick's Y axis
	/// </summary>
	/// <param name="value">The Y position of the stick</param>
	public void OnRightStickY(float value)
	{
		if (value >= rightStickYDeadZone || value <= -rightStickYDeadZone)
		{
			if (thirdPersonCamera)
			{
				if (thirdPersonCamera.gameObject.activeInHierarchy)
				{
					if (cameraIsAttached)
					{
						DetachCameraAngles();
					}
					cameraIsAttached = false;
					cameraXAngle = Mathf.Clamp(cameraXAngle + value * cameraRotationSpeedY * Time.deltaTime, cameraMinXAngle, cameraMaxXAngle);
				}
			}
			else if (firstPersonCamera.gameObject.activeInHierarchy)
			{
				firstPersonCamera.transform.Rotate(value * cameraRotationSpeedY * Time.deltaTime, 0, 0);
				Bow bow = gameObject.GetComponentInChildren<Bow>();
				if (bow)
				{
					bow.transform.Rotate(value * cameraRotationSpeedY * Time.deltaTime, 0, 0);
				}
			}
		}
	}

	/// <summary>
	/// Function bound to the left trigger
	/// </summary>
	/// <param name="value">The trigger's push value</param>
	public void OnLeftTrigger(float value)
	{
		if (value >= leftTriggerDeadZone)
		{
			cameraIsAttached = true;
		}
	}

	/// <summary>
	/// Function bound the first button on the controller.
	/// Button A for XBox.
	/// Gets called when the button is pressed.
	/// </summary>
	public void OnAction1Pressed()
	{
		if (jumpCount < maxJumpCount)
		{
			rigidbody.velocity += Vector3.up * jumpHeight;
			++jumpCount;
			//jumpButtonPressed = true;
		}
	}

	public void OnAction3Pressed()
	{
		rayDistance = 10;
		if (target)
		{
			target.Interact(this);
		}
	}

	public void OnDPadLeftPressed()
	{
		if (!sword.activeInHierarchy && !bow.activeInHierarchy)
			sword.SetActive(true);

		else if (sword.activeInHierarchy && !bow.activeInHierarchy)
		{
			sword.SetActive(false);
			bow.SetActive(true);
		}

		else if (!sword.activeInHierarchy && bow.activeInHierarchy)
			bow.SetActive(false);
	}

	public void OnDPadRightPressed()
	{
		if (!sword.activeInHierarchy && !bow.activeInHierarchy)
			bow.SetActive(true);

		else if (!sword.activeInHierarchy && bow.activeInHierarchy)
		{
			bow.SetActive(false);
			sword.SetActive(true);
		}

		else if (sword.activeInHierarchy && !bow.activeInHierarchy)
			sword.SetActive(false);
	}

	public void OnAction2Pressed()
	{
		Game.Instance().GetSpawnpool().Get(TypeID.test, Vector3.zero, 1);
	}

	public void OnAction3Released()
	{
		rayDistance = 0;
	}
	#endregion
}
