using UnityEngine;
using System;
using System.Collections;
using UnityEngine.UI;

public partial class Player : BaseAI
{
	//private Animator animationController;
	public Camera thirdPersonCamera, firstPersonCamera;
	public float playerRotationSpeed;
	public float rightStickXDeadZone, rightStickYDeadZone,
		leftStickXDeadZone, leftStickYDeadZone,
		rightTriggerDeadZone, leftTriggerDeadZone;

	// The maximum amount of times the player is allowed to jump before having to touch the ground again.
	public int maxJumpCount;

	private int jumpCount;
	private InteractableObject target;
	private float rayDistance = 10;

	public GameObject sword;
	public GameObject bow;

	public Text healthStatus;
	public Canvas canvas;

	#region cameraVariables
	public float cameraRadius;
	// For linear interpolation.
	public float cameraDamping;
	public float cameraRotationSpeedX, cameraRotationSpeedY;

	// Camera "height"
	public float cameraInitialXAngle;
	public float cameraMinXAngle, cameraMaxXAngle;
	private float cameraYAngle, cameraXAngle;
	private bool cameraIsAttached;
	#endregion

	#region animationVariables

	/*//Weapon variables
	private bool drewWeapon;
	private bool sheetWeapon;

	//Button variables
	private float leftJoyX;
	private float leftJoyY;
	private float rightTrigger;
	private bool rightTriggerReleased;
	private bool jumpButtonPressed;
	private bool blockButtonPressed;
	private bool heavyAttackButtonPressed;
	private bool stabButtonPressed;
	private bool horizontalAttackButtonPressed;
	private bool verticalAttackButtonPressed;

	//walk variables
	private float walkspeed;
	private bool isGrounded;
	private bool climbing;

	//interactableObject variables
	private bool openChest;

	//int ID's for all variables
	private int walkID, drewBowID, drewSwordID, healthID, isGroundedID, climbingID, leftJoyXID, leftJoyYID, openChestID, rightTriggerID,
				rightTriggerPressedID, sheetWeaponID, drawWeaponID, JumpButtonID, blockButtonID, HeavyAttackID, StabAttackID, HorizontalAttackID,
				verticalAttackID;
*/
	#endregion

	/// <summary>
	/// Static initializer, register factory function here
	/// </summary>
	static Player()
	{
		AIManager.RegisterFactory(TypeID.player, Factory);
	}

	/// <summary>
	/// Initialize variables here, called when the object is created.
	/// </summary>
	public override void Awake()
	{
		//DontDestroyOnLoad(this);
		//animationController = GetComponent<Animator>();
		//GenerateVariableIDs();

		InputManagerController input = Game.Instance().GetInputManager();
		input.BindAxis(InControl.InputControlType.LeftStickX, ActionBinder.Bind<Player, float>(this, (x, arg1) => x.OnLeftStickX(arg1)));
		input.BindAxis(InControl.InputControlType.LeftStickY, ActionBinder.Bind<Player, float>(this, (x, arg1) => x.OnLeftStickY(arg1)));
		input.BindAxis(InControl.InputControlType.RightStickX, ActionBinder.Bind<Player, float>(this, (x, arg1) => x.OnRightStickX(arg1)));
		input.BindAxis(InControl.InputControlType.RightStickY, ActionBinder.Bind<Player, float>(this, (x, arg1) => x.OnRightStickY(arg1)));
		input.BindAxis(InControl.InputControlType.LeftTrigger, ActionBinder.Bind<Player, float>(this, (x, arg1) => x.OnLeftTrigger(arg1)));
		input.BindAction(InControl.InputControlType.Action1, InputManagerController.State.pressed, ActionBinder.Bind<Player>(this, (x) => x.OnAction1Pressed()));
		input.BindAction(InControl.InputControlType.Action3, InputManagerController.State.pressed, ActionBinder.Bind<Player>(this, (x) => x.OnAction3Pressed()));
		input.BindAction(InControl.InputControlType.Action2, InputManagerController.State.pressed, ActionBinder.Bind<Player>(this, (x) => x.OnAction2Pressed()));
		input.BindAction(InControl.InputControlType.DPadLeft, InputManagerController.State.pressed, ActionBinder.Bind<Player>(this, (x) => x.OnDPadLeftPressed()));
		input.BindAction(InControl.InputControlType.DPadRight, InputManagerController.State.pressed, ActionBinder.Bind<Player>(this, (x) => x.OnDPadRightPressed()));

		/*Vector3 cameraPosition = transform.position;
		cameraPosition += transform.up * cameraRadius;
		thirdPersonCamera.transform.position = cameraPosition;
		thirdPersonCamera.transform.LookAt(transform);*/

		cameraIsAttached = true;
		cameraYAngle = 0f;
		cameraXAngle = cameraInitialXAngle;

		base.Awake();
	}

	private void GenerateVariableIDs()
	{
		/*
		walkID = Animator.StringToHash("WalkSpeed");
		drewBowID = Animator.StringToHash("Drew bow");
		drewSwordID = Animator.StringToHash("Drew sword");
		healthID = Animator.StringToHash("Health");
		isGroundedID = Animator.StringToHash("isGrounded");
		climbingID = Animator.StringToHash("Climbing");
		leftJoyXID = Animator.StringToHash("LeftJoyX");
		leftJoyYID = Animator.StringToHash("LeftJoyY");
		openChestID = Animator.StringToHash("Open chest");
		rightTriggerID = Animator.StringToHash("RightTrigger");
		rightTriggerPressedID = Animator.StringToHash("RightTriggerReleased");
		sheetWeaponID = Animator.StringToHash("SheetWeapon");
		drawWeaponID = Animator.StringToHash("DrawWeapon");
		JumpButtonID = Animator.StringToHash("JumpButtonPressed");
		blockButtonID = Animator.StringToHash("BlockButtonPressed");
		HeavyAttackID = Animator.StringToHash("HeavyAttackButtonPressed");
		StabAttackID = Animator.StringToHash("StabButtonPressed");
		HorizontalAttackID = Animator.StringToHash("HorizontalAttackButtonPressed");
		verticalAttackID = Animator.StringToHash("VerticalAttackButtonPressed");
		*/
	}

	/// <summary>
	/// Get the type of the player
	/// </summary>
	/// <returns>TypeID.player</returns>
	public override BaseAI.TypeID Type()
	{
		return TypeID.player;
	}


	/// <summary>
	/// Gets called when a message has been send to this by another AI
	/// </summary>
	/// <param name="sender">The AI sending the message</param>
	/// <param name="messageType">The type of the message that has been send</param>
	/// <param name="arguments">Arguments to the message, if applicable</param>
	public override void ReceiveMessage(BaseAI sender, AIManager.MessageType messageType, EventArgs arguments)
	{
		switch(messageType)
		{
			case AIManager.MessageType.damage:
			{
				health -= (arguments as DamageArguments).damage;
				if (health <= 0)
				{
					Game.Instance().GetInputManager().RemoveAllBindings();
					Game.Instance().GetWorldManager().LoadLevel(Application.loadedLevel);
				}
			}break;
		}
	}

	/// <summary>
	/// Factory function to spawn the player
	/// </summary>
	/// <returns>New instance of Player</returns>
	public static Player Factory()
	{
		return new GameObject("Player").AddComponent<Player>();
	}
	/// <summary>
	/// Used to update the camera position
	/// Gets called by Unity
	/// </summary>
	public void LateUpdate()
	{
		healthStatus.text = "Health: " + health;
		canvas.gameObject.transform.rotation = new Quaternion(0, 0, 0, 0);
		if (thirdPersonCamera)
		{
			if (thirdPersonCamera.gameObject.activeInHierarchy)
			{
				if (cameraIsAttached)
				{
					// Put the camera on the player and then move it above it.
					Vector3 cameraPosition = transform.position;
					cameraPosition.y += cameraRadius;
					thirdPersonCamera.transform.position = cameraPosition;

					// Make sure we're looking at the player.
					thirdPersonCamera.transform.LookAt(transform);

					// Calculate the angle around the player to determine the height of the camera.
					Quaternion angle = Quaternion.AngleAxis(cameraXAngle, thirdPersonCamera.transform.right);

					cameraXAngle = Mathf.LerpAngle(cameraXAngle, cameraInitialXAngle, Time.deltaTime * cameraDamping);

					thirdPersonCamera.transform.position = Quaternion.Inverse(angle) * (thirdPersonCamera.transform.position - transform.position) + transform.position;

					// Store the position of the camera and lower it to the player.
					cameraPosition = thirdPersonCamera.transform.position;
					cameraPosition.y = transform.position.y;

					// Calculate the direction between the camera and the player.
					Vector3 direction = transform.position - cameraPosition;

					// Get the relative angle (0;180) between the direction and the player's relative forward.
					float relativeAngle = Vector3.Angle(direction, transform.forward);

					// Calculate the sign(?) (thanks Google)
					float sign = Mathf.Sign(Vector3.Dot(Vector3.up, Vector3.Cross(direction, transform.forward)));

					// Use the sign to get a 360 degree angle
					float angle360 = ((relativeAngle * sign) + 180f) % 360f;

					// Move to the angle linearly interpolated to in the last frame.
					angle = Quaternion.AngleAxis(cameraYAngle, Vector3.up);
					// Linearly interpolate the camera to where the camera should be.
					cameraYAngle = Mathf.LerpAngle(cameraYAngle, -angle360 + 180f, Time.deltaTime * cameraDamping);

					// objectPostion = (objectPosition - playerPosition) * q + playerPosition
					// Where q is a quaternion of the axis and angle of the rotation you want.
					// Math ^
					// Inverse the quaternion since vector * quaternion isn't possible in unity, inversing the quaternion will give the same effect as 
					// vector * quaternion.
					thirdPersonCamera.transform.position = Quaternion.Inverse(angle) * (thirdPersonCamera.transform.position - transform.position) + transform.position;

					// Finally make sure we're looking at the player.
					thirdPersonCamera.transform.LookAt(transform);
				}
				else
				{
					// Put the camera on the player and then move it above it.
					Vector3 cameraPosition = transform.position;
					cameraPosition.y += cameraRadius;
					thirdPersonCamera.transform.position = cameraPosition;

					// Make sure we're looking at the player.
					thirdPersonCamera.transform.LookAt(transform);

					// Rotate the camera around the player over the X axis relative to the camera.
					Quaternion q = Quaternion.AngleAxis(cameraXAngle, thirdPersonCamera.transform.right);
					thirdPersonCamera.transform.position = Quaternion.Inverse(q) * (thirdPersonCamera.transform.position - transform.position) + transform.position;

					// Rotate the camera around the Y axis.
					q = Quaternion.AngleAxis(cameraYAngle, Vector3.up);
					thirdPersonCamera.transform.position = Quaternion.Inverse(q) * (thirdPersonCamera.transform.position - transform.position) + transform.position;

					// Finally make sure we're looking at the player.
					thirdPersonCamera.transform.LookAt(transform);
				}
			}
		}

		Debug.DrawLine(transform.position, transform.position + transform.forward * 10f, Color.red);

		#region animation
		/*
		animationController.SetFloat(healthID, health);
		animationController.SetBool(isGroundedID, isGrounded);
		animationController.SetBool(JumpButtonID, jumpButtonPressed);
		*/
		#endregion
	}

	private void Update()
	{
		RaycastHit hit;
		Vector3 fwd = transform.TransformDirection(Vector3.forward);
		Debug.DrawRay(transform.position, fwd * 10, Color.green);
		if (rayDistance > 0)
		{
			if (Physics.Raycast(transform.position, fwd, out hit, rayDistance))
			{
				if (hit.collider.gameObject != null)
				{
					InteractableObject interactableCollider = hit.collider.gameObject.GetComponent<InteractableObject>();
					target = interactableCollider;
				}
			}
			else
			{
				target = null;
			}
		}

		Debug.DrawLine(transform.position, transform.position + transform.forward * 10f, Color.green);

		Game.Instance().GetAIManager().SendMessageToAllOfType(this, TypeID.minion, AIManager.MessageType.tellLocation, new TellLocationArguments(transform.position));
	}

	/// <summary>
	/// Reset the camera height while detaching
	/// </summary>
	private void DetachCameraAngles()
	{
		cameraXAngle = cameraInitialXAngle;
	}

	/// <summary>
	/// Called when the object collides with another object
	/// </summary>
	/// <param name="other">The object we're colliding with</param>
	private void OnCollisionEnter(Collision other)
	{
		if(other.collider.gameObject.layer == LayerMask.NameToLayer("Terrain"))
		{
			jumpCount = 0;
			/*
			isGrounded = true;
			jumpButtonPressed = false;
			 */
		}
	}

	private void ChangeCamera()
	{
		if (thirdPersonCamera.gameObject.activeInHierarchy)
		{
			firstPersonCamera.gameObject.SetActive(true);
			thirdPersonCamera.gameObject.SetActive(false);
		}
		else if (firstPersonCamera.gameObject.activeInHierarchy)
		{
			thirdPersonCamera.gameObject.SetActive(true);
			cameraIsAttached = true;
			firstPersonCamera.gameObject.SetActive(false);
		}
	}
}
