﻿using UnityEngine;
using System.Collections;

public class BossButton : MonoBehaviour {

	public float moveDistance;
	public float moveSpeed;
	internal bool pressed;
	private Vector3 positionPos;
	public static BossButton instance;
	void Start()
	{
		instance = this;

		positionPos = transform.position;
		positionPos.y = transform.position.y - moveDistance;
	
	}
	void OnTriggerEnter(Collider other)
	{
		BaseAI o = other.gameObject.GetComponent<BaseAI>();
		if (o != null)
		{
			switch (o.Type())
			{
				case BaseAI.TypeID.player:
				{
					if (!pressed)
						pressed = true;
				} break;
			}
		}

	}
	
	// Update is called once per frame
	void Update () {
		if (pressed)
			transform.position = Vector3.MoveTowards(transform.position, positionPos, moveSpeed * Time.deltaTime);
	}
}
